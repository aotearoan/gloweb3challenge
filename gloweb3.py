import json
from datetime import date
from web3 import Web3

MORALIS_API = "https://speedy-nodes-nyc.moralis.io/fd0d8f36c9c1f2a508254528/polygon/mumbai"
WALLET_ADDRESS = ""
WALLET_PRIVATE_KEY = ""
DERC20_ABI = json.loads(open('abis/erc20.json').read())
DERC20_CONTRACT_ADDRESS = "0xfe4F5145f6e09952a5ba9e956ED0C25e3Fa4c7F1"
w3 = Web3(Web3.HTTPProvider(MORALIS_API))


def main():
  print("Showing balance for testnet wallet")
  print("MATIC balance:", w3.eth.get_balance(WALLET_ADDRESS)/1000000000000000000)
  token = w3.eth.contract(address=DERC20_CONTRACT_ADDRESS, abi=DERC20_ABI)
  print("DERC20 balance:", token.functions.balanceOf(WALLET_ADDRESS).call()/1000000000000000000)
  submitHumanInfo()


def submitHumanInfo():
  print("Going to submit verified human:", humanInfo()['user123']['name'])
  return True

def readHumanInfo():
  return False

def sendGLODistribution():
  return False


def humanInfo():
  john_doe = {
    "user123": {
      "name": "John Doe Of Denham",
      "dob": "10-10-1980",
      "nationality": "NL",
      "email": "john.doe@denham.com",
      "tel": "123",
      "cob": "abc",
      "pob": "def",
      "country":"ghi",
      "city": "jkl",
      "street": "mno",
      "nin": "pqr",
      "idn": "stu",
      "kyc_date": str(date.today()),
      "wallet": "0xD6B2fed043153cC9A4698773b8721237626ecF29",
    }
  }
  return john_doe


if __name__ == '__main__':
  main()